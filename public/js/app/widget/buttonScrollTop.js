/**
 * @author OnCode
 *
 */
define(['core/loader',
        'core/util/model'], function(Loader, Model) {
    
    'use strict';

    function ButtonScrollTop() {
        Model.call(this);
        
        this.$button = null;

        return (this);
    }
    
    ButtonScrollTop.prototype = Object.create(Model.prototype);
    
    ButtonScrollTop.prototype.init = function(obj) {
        this.$button = $(obj);

        this.click();
    };

   ButtonScrollTop.prototype.scrollToTop = function() {
        var verticalOffset = 0;
        var element = $('html');
        var offset = element.offset();
        var offsetTop = offset.top;

        $('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
    };

    ButtonScrollTop.prototype.click = function() {
        var _this = this;

        $(document).on('scroll', function(){
            if ($(window).scrollTop() > 100) {
                _this.$button.addClass('show');
            } else {
                _this.$button.removeClass('show');
            }
        });
     
        _this.$button.on('click', _this.scrollToTop);
    };

    return ButtonScrollTop;
});