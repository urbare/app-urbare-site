/**
 * @author OnCode
 *
 */
define(['core/loader', "core/widget/href", "core/util/model"], function(Loader, Href, Model) {
	
	function Print() {		
		Model.call(this);
		Href.call(this);
		
		this.iframeReference = "data-onc-printframe9999"; 
		
		return (this);
	}
	
	Print.prototype = Object.create(Model.prototype);
	
	Print.prototype.init = function(obj) {
		Href.prototype.init.call(this, obj);
	};
	
	Print.prototype.success = function(data) {
		Href.prototype.success.call(this, data);
		
		this.executePrint(data);
	};
	

	Print.prototype.executePrint = function(data) {
		$("#"+this.iframeReference).remove();
	
		iFrame = document.createElement('IFRAME');
	
		$(iFrame).attr("id", this.iframeReference)
			  .attr("name", this.iframeReference)
			  .attr("src", "about:blank")
			  .attr("scrolling", 'no')
			  .css("width", "0")
			  .css("height", "0")
			  .css("position","absolute")
			  .css("left", "-9999px")
			  .appendTo($("body:first"));
		
		if (iFrame != null) {	
			var documentToWriteTo = (iFrame.contentWindow || iFrame.contentDocument);
            
			if (documentToWriteTo.document)
                documentToWriteTo = documentToWriteTo.document;
            
            popupOrIframe = iFrame.contentWindow || iFrame;
			
            documentToWriteTo.open();
            documentToWriteTo.write(data);
            documentToWriteTo.close();
            
            iFrame.contentWindow.focus();
            iFrame.contentWindow.print();
		}
	};
	
	Print.prototype = $.extend({}, Model.prototype, Href.prototype, Print.prototype);
	
	return Print;
});