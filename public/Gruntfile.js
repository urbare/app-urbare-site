module.exports = function( grunt ) {
 
    grunt.initConfig({
        
        /**
        * SASS JS CONFIGURATIONS
        */
        sass : {
            options: {
                sourceMap: true,
                style: "compressed"
            },
            dist: {
                files: {
                    "css/all.css": "css/all.scss",
                    "css/checkout.css": "css/checkout.scss"
                }
            }
        },

        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1,
                keepSpecialComments: 0
            },
            target: {
                files: {
                    'css/all.css': 'css/all.css'
                }
            }
        },

        imagemin: {
            dynamic: {                              // Another target
                files: [{
                    expand: true,                  // Enable dynamic expansion
                    cwd: 'img/',                   // Src matches are relative to this path
                    src: ['**/*.{png,jpg,gif}'],   // Actual patterns to match
                    dest: '../dist/public/img/'    // Destination path prefix
                }]
            }
        },

        /**
        * REQUIRE JS CONFIGURATIONS
        */
        requirejs: {
            compile: {
                options: {
                    baseUrl: "./js/",
                    paths: {
                        app:  'app',
                        core: 'app/core',
                        lib:  'lib'
                    },
                    preserveLicenseComments: false,
                    dir: 'js/built',
                    modules: [{
                        name: 'app',
                        include: [
                            'app',

                            /*CORE*/

                            'core/loader',

                            'core/lib/modernizr',
                            'core/lib/async',
                            'core/lib/goog',
                            'core/lib/propertyParser',
                            'core/lib/text',
                            'core/lib/validate',
                            'core/lib/validateMethods',
                            
                            'core/util/format/url',
                            'core/util/historyLog',
                            'core/util/model',
                            'core/util/redirectUrl',
                            'core/util/reloadContent',
                            'core/util/sender',

                            'core/widget/displayContent',
                            'core/widget/fieldSubmit',
                            'core/widget/formSubmit',
                            'core/widget/href',

                            /*APP*/

                            'lib/bootstrap',
                            'lib/mask',
                            'lib/owl.carousel',
                            'lib/unveil',

                            'lib/bootstrap/affix',
                            'lib/bootstrap/alert',
                            'lib/bootstrap/button',
                            'lib/bootstrap/carousel',
                            'lib/bootstrap/collapse',
                            'lib/bootstrap/dropdown',
                            'lib/bootstrap/modal',
                            'lib/bootstrap/popover',
                            'lib/bootstrap/scrollspy',
                            'lib/bootstrap/tab',
                            'lib/bootstrap/tooltip',
                            'lib/bootstrap/transition',

                            'app/widget/buttonScrollTop',
                            'app/widget/contentCarousel',
                            'app/widget/dialog',

                            'app/view/default',
                            
                        ]
                    }]
                }
            }
        },

        clean : {
            options: { force: true },
            stuff: ['../dist/']
        },

        copy : {
            main: {
                files: [
                    {expand: true, cwd: '../', src: ['*.php'], dest: '../dist/'},
                    {expand: true, cwd: '../', src: ['*.json'], dest: '../dist/'},

                    {src: ['css/all.css'], dest: '../dist/public/', filter: 'isFile'},

                    {src: ['img/*.svg','img/*.ico','img/**/*.svg','img/**/*.ico'], dest: '../dist/public/'},

                    {expand: true, cwd: 'fonts/', src: ['**'], dest: '../dist/public/fonts/'},
                    {expand: true, cwd: '../src/', src: ['**'], dest: '../dist/src/'},
                    {expand: true, cwd: '../vendor/', src: ['**'], dest: '../dist/vendor/'},
                    {
                        expand: true,
                        cwd: 'js/built/',
                        src: [
                            'app.js',
                            'app/view/templates/*',
                            '**/html5shiv.js',
                            '**/modernizr.js',
                            '**/respond.js',
                            '**/history.js',
                            '**/requireJquery.js'
                        ],
                        dest: '../dist/public/js/'
                    }
                ]
            }
        },

        /**
        * WATCH CONFIGURATIONS
        */
        watch : {
            scripts : {
                files : [ 'css/**/*.scss', 'img/**/*' ],
                tasks : [ 'sass']
            }
        }
    });
  
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');

  // Tarefas que serão executadas
  grunt.registerTask('default', ['sass', 'cssmin', 'requirejs', 'imagemin']);
  grunt.registerTask('dist', ['sass', 'cssmin', 'requirejs', 'clean', 'copy', 'imagemin']);

  grunt.registerTask( 'w', [ 'watch' ] );
};