/**
 * @author OnCode
 *
 */
define(['core/loader',
    'core/util/model',
    'lib/owl.carousel'], function(Loader, Model) {

    'use strict';

    function ContentCarousel() {
        Model.call(this);

        this.$content = null;

        this.$leftLink = null;

        this.$rightLink = null;

        this.enablePagination = false;

        this.autoplayTime = null;

        this.sizes = [];

        return (this);
    }

    ContentCarousel.prototype = Object.create(Model.prototype);

    ContentCarousel.prototype.init = function(obj) {
        this.$content = $(obj);

        this.readDeclarative(obj);
        this.start();
    };

    ContentCarousel.prototype.readDeclarative = function(obj) {
        var sizesFull = Loader.attributeRead(obj, "data-onc-sizes", null),
            leftLink = Loader.attributeRead(obj, "data-onc-left-link", null),
            rightLink = Loader.attributeRead(obj, "data-onc-right-link", null),
            sizes = sizesFull.split(','),
            lengthSizes = sizes.length,
            i = 0;

        this.enablePagination = Loader.attributeReadBoolean(obj, "data-onc-enable-pagination", false);
        this.autoplayTime = Loader.attributeRead(obj, "data-onc-autoplay-time", false);

        for(i; i < lengthSizes; i++) {
            var splitSize = sizes[i].split('-');
            this.sizes.push(splitSize);
        }

        if(leftLink != null) {
            this.$leftLink = $(leftLink);
        }

        if(rightLink != null) {
            this.$rightLink = $(rightLink);
        }
    };

    ContentCarousel.prototype.start = function() {
        var _this = this;

        this.$content.owlCarousel({
            itemsCustom : this.sizes,
            pagination : this.enablePagination,
            paginationNumbers: false,
            autoPlay: this.autoplayTime,
            afterInit: function () {
                _this.customNavigationActions();
            },
            afterUpdate: function () {
                _this.customNavigationActions();
            }
        });

        if(this.$leftLink != null) {
            this.$leftLink.click(function () {
                _this.$content.trigger('owl.prev');
            })
        }

        if(this.$rightLink != null) {
            this.$rightLink.click(function () {
                _this.$content.trigger('owl.next');
            })
        }
    };

    ContentCarousel.prototype.customNavigationActions = function () {
        if(this.$leftLink != null) {
            var count = this.$content.find('.item').length,
                windowSize = $(window).width(),
                lengthSize = this.sizes.length,
                i = 0;

            for (i; i < lengthSize; i++) {
                if (windowSize > this.sizes[i][0]) {
                    if(count > this.sizes[i][1]) {
                        this.$leftLink.show();
                        this.$rightLink.show();
                    } else {
                        this.$leftLink.hide();
                        this.$rightLink.hide();
                    }
                }
            }
        }
    };

    return ContentCarousel;
});