define(['core/loader', 'core/util/sender', 'core/util/model'], function(Loader, Sender, Model) {
	
	function ReloadContent() {
		Model.call(this);
		
		this.url = "";
		this.method = "get";
		this.returnType = "html";
		this.replaceContent = false;
		
		this.contents = new Array();
		
		return (this);
	}
	
	ReloadContent.prototype = Object.create(Model.prototype);
	
	ReloadContent.prototype.isReplaceContent = function(element) {
		return Loader.attributeReadBoolean(element, "data-onc-reload-replace-content", this.replaceContent);
	};
	
	ReloadContent.prototype.execute = function() {
		var i, n = this.contents.length;
		
		for (i = 0; i < n; ++i) {
			var element = $(this.contents[i]);
			
			if(element) {			
				var url = Loader.attributeRead(element, "data-onc-reload-url", this.url);
				var method = Loader.attributeRead(element, "data-onc-reload-method", this.method);
				var returnType = Loader.attributeRead(element, "data-onc-reload-return-type", this.returnType);
				var replaceContent = this.isReplaceContent(element);
				
				var senderInstance = new Sender();
					
				senderInstance.method = method;
				senderInstance.urlCall = url;
				senderInstance.returnType = returnType;
				senderInstance.async = false;
				
				senderInstance.cbSuccess = function(data) {
					if(replaceContent) {
						element.replaceWith(data);
					} else {
						element.html(data);
					}
				};
				
				senderInstance.send();
			}
		};	
		
		Loader.reload();
	};
		
	return (ReloadContent);
});
