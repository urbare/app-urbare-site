/**
 * @author Jorge William Laba
 *
 */
define(['core/loader',
		'core/widget/href',
		'core/util/model',
		'lib/bootstrap/modal'], function(Loader, Href, Model) {
	
	'use strict';

	function Dialog() {
		Model.call(this);
		Href.call(this);

		return (this);
	}
	
	Dialog.prototype = Object.create(Model.prototype);

	/**
	* @param data Valor de retorno da requisicao realizada
	* Metodo responsavel por tratar resposta HTML.
	*/
	Dialog.prototype.responseHtmlSuccess = function(data) {
		this.$element.blur();
		
		$(data).on('shown.bs.modal', function() {
			Loader.reload();
		}).on('hidden.bs.modal', function (e) {
			$(this).closest('.modal').remove();
		}).modal('show');
	};

	Dialog.prototype = $.extend({}, Model.prototype, Href.prototype, Dialog.prototype);
	
	return Dialog;
});