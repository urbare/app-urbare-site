define(['lib/mustache', 'lib/bootstrap/modal'], function(Mustache, Modal) {

    'use strict';

    function reposition() {

    }

	return {
		showSuccess: function(message) {
            var _this = this;
            require(['core/lib/text!app/view/templates/message_success.html'], function(template) {
                var render = Mustache.render(template, {
                    title : 'Obrigado.',
                    message : message
                }),
                $modal = $(render);

                $modal.modal();
            });
        },
		showError: function(message) {
            var _this = this;

            require(['core/lib/text!app/view/templates/message_error.html'], function(template) {
                var render = Mustache.render(template, {
                    title: 'Verifique as informações abaixo.',
                    message: message
                }),
                $modal = $(render);

                $modal.modal();
            });
		},
		showInfo: function(message) {
        },
		showConfirm: function(title, message, labelBtnYes, labelBtnNo, callbacks) {
            require(['core/lib/text!app/view/templates/confirm.html'], function(confirmTemplate) {
                var template = $(confirmTemplate);

                template.find("#dialog-confirm-title").text(title);
                template.find("#dialog-confirm-message").html(message);
                template.find("#dialog-confirm-btn-yes").text("Sim");
                template.find("#dialog-confirm-btn-no").text("Não");

                template.find("#dialog-confirm-btn-yes").click(function() {
                    callbacks.yes();
                });

                template.find("#dialog-confirm-btn-no").click(function() {
                    callbacks.no();
                });

                template.find("#dialog-confirm-btn-cancel").click(function() {
                    callbacks.cancel();
                });

                template.modal();
            });
		}
	};
});
