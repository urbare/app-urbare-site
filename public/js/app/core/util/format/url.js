/**
 * @author OnCode
 *
 */
define(["core/loader"], function(Loader, Href, Model) {
	return {
		decodeGetParameter: function(url) {
			var args_enc, el, i, nameval, ret;
			ret = {};
			// use the DOM to parse the URL via an 'a' element
			el = document.createElement("a");
			el.href = url;
			// strip off initial ? on search and split
			args_enc = el.search.substring(1).split('&');
			
			var data = [];

			for (i = 0; i < args_enc.length; i++) {
				// convert + into space, split on =, and then decode 
				args_enc[i].replace(/\+/g, ' ');
				nameval = args_enc[i].split('=', 2);
				ret[decodeURIComponent(nameval[0])]=this.decodeGetValue(nameval[1]);
			}
			
			return ret;
		},

		decodeGetValue: function(value) {
			value = (value === null) ? '' : value;
				
			value = value.replace(/\r?\n/g, '\r\n');
			value = decodeURIComponent(value);
			value = value.replace(/%20/g, '+');

			return value;
		},

		addParameter: function(url, paramName, paramValue) {
			if(url.indexOf("?") == -1) {
				url += '?' + paramName + '=' + paramValue;
			} else {
				url += '&' + paramName + '=' + paramValue;
			}

			return url;
		},

		getParameterLine: function(url) {
			if(url.indexOf("?") == -1) {
				return '';
			} else {
				return url.substr(url.indexOf("?")+1, url.length);
			}
		},

		getWithOutParameter: function(url) {
			return url.replace(this.getParameterLine(url),'').replace("?",'');
		}
	};
});