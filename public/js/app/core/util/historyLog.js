define(['core/loader', 'core/util/sender', 'core/util/model', 'core/util/format/url'], function(Loader, Sender, Model, UrlFormat) {
	
	function HistoryLog() {
		Model.call(this);
		
		this.url = "";
		this.method = "get";
		this.returnType = "html";

		return (this);
	}
	
	HistoryLog.prototype = Object.create(Model.prototype);
	
	HistoryLog.prototype.init = function() {
		var _this = this;
		
		var hash = $.trim(window.location.hash).replace('#','');
		var loc = window.history.location || document.location;
		
		$('a[href="#"]').attr('href', 'javascript:void(0)');

		if(hash != '') {
			window.location.replace(_this.getBasePageUrl() + hash);
			return;	
		}

		$(window).bind('popstate', function( event ) {
			var loc = window.history.location || document.location;

			_this.url = loc;
			_this.execute();
		});
	};
	
	HistoryLog.prototype.getBasePageUrl = function() {
		return window.location.href.replace(/[#\?].*/,'').replace(/[^\/]+$/,function(part,index,string){
			return (/[^\/]$/).test(part) ? '' : part;
		}).replace(/\/+$/,'');
	};
	
	HistoryLog.prototype.execute = function() {
		var url = this.url.href;

		var senderInstance = new Sender();

		if(url.indexOf('#') != -1)
			url = url.replace('#','');

		senderInstance.method = this.method;	
		senderInstance.urlCall = UrlFormat.addParameter(url, 'isBackBrowserAction', true);
		senderInstance.returnType = this.returnType;
		
		senderInstance.cbSuccess = function(data) {
			var body = data.replace(/^[\s\S]*<body.*?>|<\/body>[\s\S]*$/g, '');
			
			$("body").html(body);
			
			Loader.reload();
		};

		senderInstance.send();
	};
		
	return (HistoryLog);
});
