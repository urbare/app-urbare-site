/**
 * @author OnCode
 *
 */
define(['core/loader',
		'core/util/sender',
		'core/util/reloadContent',
		'core/util/model'], function(Loader, Sender, ReloadContent, Model) {
	
	function FieldSubmit() {
		Model.call(this);
		
		/**
		* Elemento principal, responsavel pelas informacoes da classe
		*/
		this.field = null;
		
		/**
		* Substitui o nome do parametro enviado, caso o mesmo nao deva ser o nome do campo.
		*/
		this.fieldName = null;

		/**
		* Caso seja necessario enviar outros valores, esse campo e populado com um array separado por virgula
		*/
		this.extraFields = null;

		/**
		* Identifica a url para qual sera enviada a requisicao
		*/
		this.urlCall = null;

        /**
         * Lista de paginas a serem recarregadas posterior ao formulario ser submetido.
         */
        this.reloadContents = [];

		/**
		* Tipo de requisicao
		*/
		this.method = "post";
		
		/**
		* Identifica se ao apertar o enter o evento sera ativado
		*/
		this.enterEnable = true;
		
		/**
		* Referencia algum botao em especifico.
		*/
		this.referenceButton = null;
		
		/**
		* Evento que ira disparar a acao
		*/
		this.attachEvent = null;
				
		/**
		* TIpo de Retorno
		*/
		this.returnType = "json";
		
		/**
		* Especifica um output para o retorno. (Div)
		*/
		this.dataContent = "";
		
		/**
		* Identifica se a div de ouput sera substituida ou somente seu conteudo sera trocado
		*/
		this.replaceContent = false;

		/**
		 * Declara se o loading padrão está ativo ou não
		 */
		this.enableLoading = true;

		return (this);
	}
	
	FieldSubmit.prototype = Object.create(Model.prototype);
	
	/**
	* @param obj Objeto principal, injetado pela classe loader. Representa o elemento no qual ira ser manipulado
	* Metodo construtor.
	*/
	FieldSubmit.prototype.init = function(obj) {
		this.readDeclarative(obj);
		
		this.event();
	};
	
	/**
	* @param obj Elemento no qual serao extraidas as informacoes.
	* Metodo responsavel por ler as tag's e preencher os atributos da classe
	*/
	FieldSubmit.prototype.readDeclarative = function(obj) {
		var objReloadContents = Loader.attributeRead(obj, 'data-onc-reload-contents', null);

        this.field = obj;

		this.urlCall = Loader.attributeRead(obj, "data-onc-url-call", this.urlCall);
		this.fieldName = Loader.attributeRead(obj, "data-onc-field-name", this.fieldName);
		this.extraFields = Loader.attributeReadList(obj, "data-onc-extra-fields", this.extraFields);
		this.method = Loader.attributeRead(obj, "data-onc-method", this.method);
		this.referenceButton = Loader.attributeRead(obj, "data-onc-reference-button", this.referenceButton);
		this.attachEvent = Loader.attributeRead(obj, "data-onc-attach-event", this.attachEvent);
		this.returnType = Loader.attributeRead(obj, "data-onc-return-type", this.returnType);
		this.dataContent = Loader.attributeRead(obj, "data-onc-data-content", this.dataContent);
		this.replaceContent = Loader.attributeReadBoolean(obj, "data-onc-replace-content", this.replaceContent);
		this.enableLoading = Loader.attributeReadBoolean(obj, "data-onc-enable-loading", this.enableLoading);

		if(objReloadContents !== null) {
            this.reloadContents = objReloadContents.split(',');
        }
	};
		
	/**
	* Metodo chamado antes da requisicao ser enviada
	*/
	FieldSubmit.prototype.beforeSend = function() {
		return true;
	};
	
	FieldSubmit.prototype.enterSubmit = function() {
        if(this.attachEvent != null) {
            $(this.field).trigger(this.attachEvent);
        } else {
            this.logicSubmit();
        }
	};
	
	/**
	* @param data Valor de retorno da requisicao realizada
	* Metodo responsavel por tratar resposta JSON com sucesso.
	*/
	FieldSubmit.prototype.responseJsonSuccess = function(data) {
	};
	
	/**
	* @param data Valor de retorno da requisicao realizada
	* Metodo responsavel por tratar resposta JSON com error.
	*/
	FieldSubmit.prototype.responseJsonError = function(data) {
	};

	/**
	* @param data Valor de retorno da requisicao realizada
	* Metodo responsavel por tratar resposta HTML com sucesso
	*/
	FieldSubmit.prototype.responseHtmlSuccess = function(data) {
	};
		
	/**
	* @param data Valor de retorno da requisicao realizada
	* Metodo responsavel por tratar o retorno com sucesso
	*/
	FieldSubmit.prototype.success = function(data) {
		if(this.returnType == 'json') {
            this.responseJsonSuccess(data);
		} else if (this.returnType == 'html') {
			if(this.dataContent != '') {
				if(this.replaceContent == true) {
					$(this.dataContent).replaceWith(data);
				} else {
					$(this.dataContent).html(data);				
				}

                if(this.reloadContents === null || this.reloadContents.length <= 0) {
                    Loader.reload();
                }
			}

			this.responseHtmlSuccess(data);
		}
	};

    FieldSubmit.prototype.error = function(data, xhr, ajaxOptions, thrownError) {
        if(this.returnType == 'json' && xhr.status === 400) {
            this.responseJsonError(data);
        }
    };
	
	/**
	* Metodo responsavel por tratar os eventos
	*/
	FieldSubmit.prototype.event = function() {
		var _this = this;
		var _field = $(_this.field);

		if(_this.enterEnable == true 
			&& _field.prop("tagName").toLowerCase() == 'input' 
			&& _field.attr("type").toLowerCase() != 'radio' 
			&& _field.attr("type").toLowerCase() != 'checkbox') {
			_field.on('keypress', function(e) {
				if (e.keyCode == 13) {
					_this.enterSubmit();
					return false;
				}
			});
		}
		
		if(_this.referenceButton != null) {
			$(_this.referenceButton).on("click", function() {
				_this.logicSubmit();
				return false;
			});
		}
		
		if(_this.attachEvent != null) {
			_field.on(this.attachEvent, function(e) {
				_this.logicSubmit();
				
				//Caso seja qualquer tipo de input o mesmo deve sempre retornar true, para confirmar a acao
				if(_field.prop("tagName").toLowerCase() == 'input' 
					&& _field.attr("type").toLowerCase() != 'submit' ) {
					return true;	
				}

				return false;
			});
		}
	};

    /**
     * Metodo responsavel por exeutar a atualizacao dos campos pos processamento
     */
    FieldSubmit.prototype.executeReloadContents = function() {
        if(this.reloadContents !== null) {
            var reloadContentInstance = new ReloadContent();

            reloadContentInstance.contents = this.reloadContents;
            reloadContentInstance.execute();
        }
    };
	
	/**
	* Metodo responsavel por tratar valores antes de submeter a execucao.
	* Esse metodo e chamado sempre que algum evento e disparado.
	*/
	FieldSubmit.prototype.beforeExecute = function() {
	};

	/**
	* Metodo que executa o envio da requisicao para o servidor.
	*/
	FieldSubmit.prototype.logicSubmit = function() {
		var _this = this;
		var _field = $(_this.field);

		_this.beforeExecute();

		var senderInstance = new Sender();
		
		var data = this.createFieldParameter(_field);

		senderInstance.method = _this.method;
		senderInstance.urlCall = Loader.attributeRead(_field, "data-onc-url-call", _this.urlCall);
		senderInstance.data = _this.serializeExtraFields(data);
		senderInstance.returnType = _this.returnType;
		senderInstance.loading = _this.enableLoading;

		senderInstance.cbBeforeSend = function() {
			return _this.beforeSend.call(_this);
		};

        senderInstance.cbSuccess = function(data) {
            _this.success.call(_this, data);
            _this.executeReloadContents.call(_this);
        };

        senderInstance.cbError = function(data, xhr, ajaxOptions, thrownError) {
            _this.error.call(_this, data, xhr, ajaxOptions, thrownError);
        };
		
		senderInstance.send();
	};

    FieldSubmit.prototype.createFieldParameter = function($field) {
        var data = null;

        if(this.fieldName !== null) {
            data = this.fieldName + "=" + $field.val();
        } else {
            data = $field.attr("name") + "=" + $field.val();
        }

        return data;
    };

	FieldSubmit.prototype.serializeExtraFields = function(data) {
        if(this.extraFields !== null && this.extraFields.length > 0) {
			var i, n = this.extraFields.length;

			for (i = 0; i < n; ++i) {
				var $element = $(this.extraFields[i]),
					value = '';

				if($element.prop("tagName") == 'input' && $element.attr("type") == 'radio') {
					value = $element.filter(':checked').val();
				} else {
					value = $element.val();
				}

				data += "&" + $element.attr('name') + "=" + value;
			}
		}

		return data;
	};
	
	return FieldSubmit;
});