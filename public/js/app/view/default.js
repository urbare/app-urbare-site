/*
 * @author Jorge William Laba
 *
 */
define(['core/loader',
        'lib/bootstrap/collapse',
        'lib/bootstrap/dropdown',
        'lib/bootstrap/tooltip',
        'lib/bootstrap/tab',
        'lib/bootstrap/carousel',
        'lib/bootstrap/transition'], function(Loader) {
    return {
        init: function(objContext) {
            $('[data-toggle="tooltip"]', objContext).tooltip();

            require(['lib/unveil'], function(mask) {
                $("img").unveil(200, function () {
                    $(this).load(function () {
                        this.style.opacity = 1;
                    });
                });
            });

            $(window).scroll(function() {
                var scroll = $(window).scrollTop();

                if (scroll <= 94) {
                    $(".tpl-header-navbar").removeClass("tpl-header-navbar-show");
                } else {
                    $(".tpl-header-navbar").addClass("tpl-header-navbar-show");
                }
            });

            $('a.animated-anchor').click(function(){
                $('html, body').animate({
                    scrollTop: $( $.attr(this, 'href') ).offset().top-80
                }, 500);
                return false;
            });

            require(['lib/fancybox'], function(mask){
                $(".fancybox-button").fancybox({
                    closeBtn		: false,
                    helpers		: {
                        title	: { type : 'inside' },
                        buttons	: {}
                    }
                });
            });

            require(['lib/mask'], function(mask) {
                var SPMaskBehavior = function (val) {
                        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
                    },
                    spOptions = {
                        onKeyPress: function(val, e, field, options) {
                            field.mask(SPMaskBehavior.apply({}, arguments), options);
                        }
                    };

                $('.input-imei', objContext).mask("000000000000000");
                $('.input-cpf', objContext).mask("000.000.000-00");
                $('.input-cnpj', objContext).mask("00.000.000/0000-00");
                $('.input-phone', objContext).mask(SPMaskBehavior, spOptions);
                $('.input-telphone', objContext).mask("(00) 0000-0000");
                $('.input-ie', objContext).mask("00.000.0000-0");
                $('.input-zipcode', objContext).mask("00000-000");
                $('.input-date', objContext).mask("00/00/0000");
                $('.input-creditcard-expiry', objContext).mask("00/00");
                $('.input-creditcard-safety', objContext).mask("000");
                $("input[type='tel']", objContext).mask(SPMaskBehavior, spOptions);
                $("input[type='date']", objContext).mask("00/00/0000");
            });
        }
    };
});