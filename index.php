<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';

$app = new \Slim\App;

$container = $app->getContainer();

$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('src/templates', [
        'cache' => false
    ]);
    
    $view->addExtension(new \Slim\Views\TwigExtension(
        $container['router'],
        $container['request']->getUri()
    ));

    $view->getEnvironment()->addGlobal('baseUrl', 'http://localhost:8080/');

    return $view;
};

$app->get('/', function (Request $request, Response $response) {
    $response = $this->view->render($response, "index.phtml", []);
    return $response;
});

$app->get('/modal-contato', function (Request $request, Response $response) {
    $response = $this->view->render($response, "modal-contact.phtml", []);
    return $response;
});

$app->post('/enviar-email', function (Request $request, Response $response) {

    $data = $request->getParsedBody();
    $mail = new PHPMailer;

    $mail->isSMTP();
    $mail->Host = "email-smtp.us-east-1.amazonaws.com";
    $mail->SMTPAuth = true;
    $mail->Username = 'AKIAJTBDGV7VB7DVGUYA';
    $mail->Password = 'AiHKl4y0w0Xc9AbP+FdRicq8yo12TsfDzhmtnNHtWRD3';
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;

    $mail->setFrom('atendimento@agregabr.com.br', 'Agrega');
    $mail->addAddress('teste.email.com', 'Teste Nome');

    $mail->isHTML(true);

    $mail->Subject = 'Atendimento Urbare ' . $data['name'];
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
    $mail->Body    = '<p>Nome: '. $data['name'].'</p>'.
                     '<p>E-mail: '. $data['email'].'</p>'.
                     '<p>Telefone:'.$data['phone'].'</p>'.
                     '<p>Mensagem:'. $data['message'].'</p>';

    if(!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        echo 'Message has been sent';
    }
});


$app->run();