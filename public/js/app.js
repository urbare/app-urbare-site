
/**
 * Configuracao dos diretorios bases do javascript
 **/
requirejs.config({
	urlArgs: 'v=' + ((ENVIRONMENT != 'dev') ? '102' : (new Date()).getTime()),
    appDir: '../',
    baseUrl: BASE_URL + ((ENVIRONMENT != 'dev') ? '/js/built/' : '/js/'),
    waitSeconds: 0,
    shim: {
        'modernizr': {
            exports: 'Modernizr'
        }
    },
    paths: {
        app:  'app',
        core: 'app/core',
        lib:  'lib',
        modernizr: 'app/core/lib/modernizr'
    }
});

/**
 * Carrega somente os pacotes necessarios para utilizacao do sistema
 **/
requirejs(["core/loader", "core/util/historyLog"], function(Loader, HistoryLog) {
	$(document).ready(function() {
        try {
			Loader.init({
                senderLoading: 'app/view/helper/loading',
                senderCatchError: 'app/view/helper/catchError'
            });
			
			historyLog = new HistoryLog();
			historyLog.init();
		} catch(e) {
            alert("error");
			console.log(e);
		}
	});
});
