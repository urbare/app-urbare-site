define(['core/loader',
		'core/util/model'], function(Loader, Model) {
	
	"use strict";

	function Sender() {
		Model.call(this);
		
		this.urlCall = '';
		this.method = 'get';
		this.returnType = 'html';
		this.data = null;
		this.async = true;
		this.loading = true;
		this.cache = true;
		this.processData = true;
		this.contentType = 'application/x-www-form-urlencoded; charset=UTF-8';
		this.xhrFields = null;

		this.cbSuccess = null;
		this.cbError = null;
		this.cbBeforeSend = null;
		this.cbComplete = null;
		
		return (this);
	}

	Sender.prototype = Object.create(Model.prototype);
	
	Sender.prototype.success = function(data) {
		if(this.cbSuccess !== null) {
			this.cbSuccess.call(this, data);
		}
	};
	
	Sender.prototype.error = function(xhr, ajaxOptions, thrownError) {
		var _this = this,
            data = null;

		if(xhr.status !== 400 && Loader.getInitConfig().senderCatchError !== null) {
			require([Loader.getInitConfig().senderCatchError], function (SenderCatchError) {
                if (xhr.status === 0) {
                    SenderCatchError.error0(_this.urlCall, xhr.responseText, _this.data);
                } else if (xhr.status == 404) {
                    SenderCatchError.error404(_this.urlCall, xhr.responseText, _this.data);
                } else if (xhr.status == 401) {
                    SenderCatchError.error401(_this.urlCall, xhr.responseText, _this.data);
                } else if (xhr.status == 403) {
                    SenderCatchError.error403(_this.urlCall, xhr.responseText, _this.data);
                } else if (xhr.status == 500) {
                    SenderCatchError.error500(_this.urlCall, xhr.responseText, _this.data);
                } else {
                    SenderCatchError.error(_this.urlCall, xhr.responseText, _this.data, thrownError);
                }
            });
        } else if (_this.returnType == 'json' && xhr.status == 400) {
            data = JSON.parse(xhr.responseText);
        }

		if(this.cbError !== null) {
			this.cbError.call(this, data, xhr, ajaxOptions, thrownError);
		}
	};
	
	Sender.prototype.beforeSend = function() {
		var isOk = true;

		if(this.loading && typeof Loader.getInitConfig().senderLoading !== 'undefined' &&
			Loader.getInitConfig().senderLoading !== null) {
			
			require([Loader.getInitConfig().senderLoading], function(SenderLoading) {
				SenderLoading.show();
			});
		}
		
		if(this.cbBeforeSend !== null) {
			isOk = this.cbBeforeSend.call(this);
		}
		
		if(!isOk && typeof Loader.getInitConfig().senderLoading !== 'undefined' &&
			Loader.getInitConfig().senderLoading !== null) {
			
			require([Loader.getInitConfig().senderLoading], function(SenderLoading) {
				SenderLoading.hide();
			});
		}

		return isOk;
	};
	
	Sender.prototype.complete = function() {
		if(this.loading && typeof Loader.getInitConfig().senderLoading !== 'undefined' &&
			Loader.getInitConfig().senderLoading !== null) {
			
			require([Loader.getInitConfig().senderLoading], function(SenderLoading) {
				SenderLoading.hide();
			});
		}
		
		if(this.cbComplete !== null) {
			this.cbComplete.call(this);
		}
	};
	
	Sender.prototype.send = function() {
		var _this = this;
		
		if(_this.beforeSend()) {
			$.ajax({
				url: _this.urlCall,
				type: _this.method,
				dataType: _this.returnType,
				data: _this.data,
				async: _this.async,
				cache: _this.cache,
				xhrFields: _this.xhrFields,
				processData: _this.processData,
				contentType: _this.contentType,
				complete: function() {
					_this.complete();
				},
				error: function(xhr, ajaxOptions, thrownError) {
                    _this.error(xhr, ajaxOptions, thrownError);
				},
				success: function(data) {
					_this.success(data);
				}
			});
		}
	};
	
	return (Sender);
});