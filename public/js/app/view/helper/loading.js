define(function() {

	var loader = null;

	return {
        show: function(title, message) {
            require(['lib/blockui'], function(blockui) {
                $.blockUI({
                    message: $('#tpl-loading-center').html(),
                    baseZ: 9999999,
                    css: {
                        width: '160px',
                        height: '130px',
                        zIndex: 9999999,
                        border: 'none',
                        top: '50%',
                        left: '50%',
                        background: 'transparent',
                        marginLeft: '-80px',
                        marginTop: '-65px'
                    }
                });
            });
        },
        hide: function(title, message) {
            require(['lib/blockui'], function(blockui) {
                $.unblockUI();
            });
        }
	};
});
