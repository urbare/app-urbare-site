/**
 * @author OnCode
 *
 */
define(['core/loader',
		'core/util/sender',
		'core/util/reloadContent',
		'core/util/redirectUrl',
		'core/util/model'], function(Loader, Sender, ReloadContent, RedirectUrl, Model) {
	
	'use strict';

	function FormSubmit() {
		Model.call(this);

		/**
		* Elemento principal anotado.
		*/
		this.$element = null;

		/**
		* Formulario a ser submetido
		*/
		this.$form = null;

		/**
		* Botao responsavel por ativar o evento de submeter o formulario
		*/
		this.$button = null;

		/**
		* Especifica um output para o retorno. (Div)
		*/
		this.$dataContent = null;

		/**
		* Elemento identifica a navegacao ajax
		*/
		this.hash = null;

		/**
		* Verifica se o formulario esta disponivel para ser submetido.
		*/
		this.available = true;

		/**
		* Evento que ira disparar o form submit
		*/
		this.attachEvent = 'click';

		/**
		* Lista de paginas a serem recarregadas posterior ao formulario ser submetido.
		*/
		this.reloadContents = [];

		/**
		* Atributo identifica se o formulario esta sendo processado. Caso sim
		* o botao e desativado.
		*/
		this.lockedSubmit = 'data-onc-disabled';

		/**
		* Referencia um formulario. Utilizado quando o botao nao esta dentro da tag <form>
		*/
		this.referenceform = null;
		
		/**
		* Em caso de sucesso o formulario sera resetado para o padrao
		*/
		this.enableFormReset = false;
		
		/**
		* Retorno da url na qual o formulario foi submetido
		*/
		this.returnType = 'json';
		
		/**
		* Identifica se a div de ouput sera substituida ou somente seu conteudo sera trocado
		*/
		this.replaceContent = false;
		
		/**
		* Formulario sera submetido via ajax ou requisicao normal.
		*/
		this.isAjax = true;
		
		return (this);
	}
	
	FormSubmit.prototype = Object.create(Model.prototype);
	
	/**
	* @param obj Objeto principal, injetado pela classe loader. Representa o elemento no qual ira ser manipulado
	* Metodo construtor.
	*/
	FormSubmit.prototype.init = function(obj) {
		this.readDeclarative(obj);
		
		this.submit();
	};
	
	/**
	* @param obj Elemento no qual serao extraidas as informacoes.
	* Metodo responsavel por ler as tag's e preencher os atributos da classe
	*/
	FormSubmit.prototype.readDeclarative = function(obj) {
		var $obj = $(obj),
			objReloadContents = Loader.attributeRead(obj, 'data-onc-reload-contents', null),
			dataContent = Loader.attributeRead(obj, 'data-onc-data-content', this.$dataContent);
		
		//Caso o elemento obj seja do tipo form, o botao utilizado e o form referencia nao sao necessarios
		if($obj.prop('tagName').toUpperCase() === 'FORM') {
			this.$form = $obj;
		} else {
			this.$button = $obj;
			this.referenceform = Loader.attributeRead(obj, 'data-onc-reference-form', this.referenceform);
		}

		//Realiza a logica para verificar os botoes responsaveis por submeter o formulario
		if(typeof this.$form === 'undefined' || this.$form === null) {
			//Caso nao exista formulario de referencia, ele ira pegar o primeiro elemento form pai.
			if(this.referenceform === null) {
				this.$form = this.$button.closest('form');
			} else {
				this.$form = $(this.referenceform);
			}
		} else {
			//Caso o proprio formulario tenha sido anotado, somente os campos com a tag abaixo, poderao submeter o formulario
			var submitButtons = this.$form.find('[data-onc-btn-submit="true"]');

			if(typeof submitButtons !== 'undefined' && submitButtons.length > 0) {
				this.button = submitButtons;
			}
		}

		if(objReloadContents !== null) {
			this.reloadContents = objReloadContents.split(',');
		}
		
		if(dataContent !== null) {
			this.$dataContent = $(dataContent);
		}

		this.$element = $obj;
		
		this.isAjax = Loader.attributeRead(obj, 'data-onc-is-ajax', this.isAjax);
		this.hash = Loader.attributeRead(obj, 'data-onc-hash', this.hash);
		this.replaceContent = Loader.attributeReadBoolean(obj, 'data-onc-replace-content', this.replaceContent);
		this.enableFormReset = Loader.attributeReadBoolean(obj, 'data-onc-enable-form-reset', this.enableFormReset);
		this.attachEvent = Loader.attributeRead(obj, 'data-onc-attach-event', this.attachEvent);
		this.returnType = Loader.attributeRead(obj, 'data-onc-return-type', this.returnType);
	};
	
	/**
	* Metodo le em tempo de execucao a tag data-onc-available, responsavel
	* por dizer se o formulario esta disponivel para ser submetido ou nao.
	*/
	FormSubmit.prototype.isAvailable = function() {
		return Loader.attributeReadBoolean(this.button, 'data-onc-available', this.available);
	};
	
	/**
	* Metodo chamado antes do formulario ser enviado
	* 
	*/
	FormSubmit.prototype.beforeSend = function() {
		return true;
	};
	
	/**
	* Metodo chamado antes do formulario ser serializado
	*/
	FormSubmit.prototype.beforeSerialize = function() {};
	
	/**
	* Metodo chamado apos a requisicao ser completada.
	*/
	FormSubmit.prototype.complete = function() {};

	/**
	* Metodo ativa o formulário para ser executado novamente.
	*/
	FormSubmit.prototype.unlockForm = function() {
		this.$form.removeClass(this.lockedSubmit);
	};

	/**
	* Metodo responsavel por disparar a acao de submissao do formulario.
	*/
	FormSubmit.prototype.enterSubmit = function() {
		if(typeof this.$button !== 'undefined' && this.$button !== null) {
			this.$button.trigger(this.attachEvent);
		} else {
			this.logicSubmit();
		}
	};
	
	/**
	* Funcao responsavel por realizar o reset de todos os campos do formulario.
	*/
	FormSubmit.prototype.resetForm = function() {
		this.$form[0].reset();
		
		//Acao chamada em caso de plugins.
		this.$form.find('select').change();
	};

	/**
	* @param data Valor de retorno da requisicao realizada
	* Metodo responsavel por tratar a resposta da requisicao.
	*/
	FormSubmit.prototype.success = function(data) {
		if(this.returnType == 'json') {
            if(this.executeRedirectUrl(data)) {
                return;
            }

            if(this.enableFormReset) {
                this.resetForm();
            }

            this.responseJsonSuccess(data);
		} else if (this.returnType === 'html') {
			if(typeof this.$dataContent !== 'undefined' &&  this.$dataContent !== null) {
				if(this.replaceContent) {
					this.$dataContent.replaceWith(data);
				} else {
					this.$dataContent.html(data);
				}
				
				if(this.reloadContents === null || this.reloadContents.length <= 0) {
					Loader.reload();
				}
			}
			
			this.responseHtmlSuccess(data);
		}

		this.unlockForm();
		
		return true;
	};

    /**
     * Metodo responsavel por tratar a resposta da requisicao com erro.
     *
     * @param data Valor de retorno da requisicao realizada
     * @param xhr
     * @param ajaxOptions
     * @param thrownError
     */
    FormSubmit.prototype.error = function(data, xhr, ajaxOptions, thrownError) {
        if(this.returnType == 'json' && xhr.status === 400) {
            this.responseJsonError(data);
        }
    };

	/**
	* @param data Valor de retorno da requisicao realizada
	* Metodo responsavel por tratar resposta HTML.
	*/
	FormSubmit.prototype.responseHtmlSuccess = function(data) {
	};
	
	/**
	* @param data Valor de retorno da requisicao realizada
	* Metodo responsavel por tratar resposta JSON com sucesso.
	*/
	FormSubmit.prototype.responseJsonSuccess = function(data) {
	};
	
	/**
	* @param data Valor de retorno da requisicao realizada
	* Metodo responsavel por tratar resposta JSON com error.
	*/
	FormSubmit.prototype.responseJsonError = function(data) {
	};

	/**
	* @param data Valor de retorno da requisicao realizada
	* @p
	* Metodo responsavel por identificar se a requisicao ajax foi realizada com sucesso
	*/
	FormSubmit.prototype.responseJsonRedirect = function(data) {};
	
	/**
	* Metodo responsavel por exeutar a atualizacao dos campos pos processamento
	*/
	FormSubmit.prototype.executeReloadContents = function() {
		if(this.reloadContents !== null) {
			var reloadContentInstance = new ReloadContent();
			
			reloadContentInstance.contents = this.reloadContents;
			reloadContentInstance.execute();
		}
	};

	/**
	* @param data Retorno da requisicao efetuada
	* @return Caso o elemento seja 
	* Metodo responsavel por verificar se e necessario o redirecionamento
	*/
	FormSubmit.prototype.executeRedirectUrl = function(data) {
		var redirectUrlInstance = new RedirectUrl();

		this.responseJsonRedirect(data);
		
		if(typeof data !== 'undefined' && data !== null) {
			if(typeof data.redirectUrl !== 'undefined' && data.redirectUrl !== null) {
				redirectUrlInstance.url = data.redirectUrl;
			}
			
			if(typeof data.redirectHash !== 'undefined' && data.redirectHash !== null) {
				redirectUrlInstance.hash = data.redirectHash;
			}
			
			if(typeof data.redirectIsAjax !== 'undefined' && data.redirectIsAjax !== null) {
				redirectUrlInstance.isAjax = data.redirectIsAjax;
			}
		}

		if(redirectUrlInstance.hasRedirect(this.$element)) {
			redirectUrlInstance.execute(this.$element);
		
			if(!redirectUrlInstance.isAjax) {
				return true;
			}
		}

		return false;
	};
	
	/**
	* Metodo responsavel por tratar os eventos
	*/
	FormSubmit.prototype.submit = function() {
		var _this = this;

		//Cancela a requisicao padrao caso seja ajax.
		if(this.isAjax) {
			this.$form.on('submit', false);
		}
		
		//Configura a requisicao ajax, em caso de enter nos campos textos.
		this.$form.find('input, select').on('keypress', function(e) {
			if (e.keyCode == 13) {
				_this.enterSubmit();
				return false;
			}
		});
		
		//Inclui o evento padrao configurado no elemento botao
		if(typeof this.$button !== 'undefined' && this.$button !== null) {
			this.$button.on(_this.attachEvent, function(e) {
				_this.logicSubmit();
				return _this.isInputRadioButton(e);
			});
		}
	};
	
	/**
	* Fix input radio ie 8 and ie 7 return false. 
	*/
	FormSubmit.prototype.isInputRadioButton = function(e) {
		var domElement = $(e.target);

		if(domElement.is('input') && domElement.attr('type') == 'radio') {
			return true;
		}

		return false;
	};

	/**
	* Metodo responsavel por submeter o formulario.
	*/
	FormSubmit.prototype.logicSubmit = function() {
		var _this = this;
		
		if(this.isAvailable() === false) {
			return false;
		}

		/*
		if(_this.$form.hasClass(_this.lockedSubmit) || _this.isAvailable() === false) {
			return false;
		} else {
			_this.$form.addClass(_this.lockedSubmit);
		}*/

		if(_this.isAjax === true) {
			_this.beforeSerialize();

			var senderInstance = new Sender(),
				data = _this.$form.serialize();
			
			senderInstance.method = _this.$form.attr('method');
			senderInstance.urlCall = _this.$form.attr('action');
			senderInstance.data = data;
			senderInstance.returnType = _this.returnType;
			
			if(_this.hash !== null && _this.$form.attr('method') === 'get') {
				var hashUrl = _this.hash;

				if(hashUrl.indexOf('?') == -1) {
					hashUrl += '?';
				} else {
					hashUrl += '&';
				}

				window.history.pushState(null, null, hashUrl + data);
			}

			senderInstance.cbBeforeSend = function() {
				return _this.beforeSend.call(_this);
			};
			
			senderInstance.cbComplete = function() {
				_this.complete.call(_this);
			};

            senderInstance.cbSuccess = function(data) {
                _this.success.call(_this, data);
                _this.executeReloadContents.call(_this);
            };

            senderInstance.cbError = function(data, xhr, ajaxOptions, thrownError) {
                _this.error.call(_this, data, xhr, ajaxOptions, thrownError);
            };
			
			senderInstance.send();
		} else {
			if(_this.beforeSend()) {
				_this.$form[0].submit();
			}
		}
	};
	
	return FormSubmit;
});