define(function() {
    return {
        addError: function(element, error) {
            var $element     = $(element),
                $formRow     = $element.closest('.form-group'),
                $formMessage = $formRow.find('.help-block'),
                $error = $(error),
                $formControlFeedback = $formRow.find('.form-control-feedback');

            if($formRow !== null && typeof $formRow !== 'undefined' &&
                error !== '' && $error.text() !== '') {

                if($formMessage.length === 0) {
                    var $createMessage = $('<div class="help-block-validator help-block"></div>'),
                        $iconValidate = $('<span class="form-control-feedback"></span>');
                    
                    $formRow.append($createMessage);
                    $formRow.append($iconValidate);
                    
                    $formControlFeedback = $formRow.find('.form-control-feedback');
                    $formMessage = $formRow.find('.help-block-validator');
                }

                $formRow.removeClass("has-success has-feedback")
                        .addClass("has-error has-feedback");

                $formControlFeedback.removeClass('fa fa-check')
                                    .addClass('fa fa-close');

                $formMessage.append(error);
            } else {
                $formMessage.find("[for='"+ $element.attr("name") +"'], [for='"+ $element.attr("id") +"']").remove();
            }
        },
        addSuccess: function(element) {
            var $element     = $(element),
                $formRow     = $element.closest('.form-group'),
                $formMessage = $formRow.find('.help-block'),
                $formControlFeedback = $formRow.find('.form-control-feedback');
            
            if($formRow !== null && typeof $formRow !== 'undefined') {
                if($formMessage.length !== 0) {
                    $formMessage = $formRow.find('.help-block-validator');

                    $formMessage.find("[for='"+ $element.attr("name") +"'], [for='"+ $element.attr("id") +"']")
                                  .remove();

                    if(!$formMessage.children()[0]) {
                        $formMessage.remove();
                    }
                    
                    $formRow.removeClass("has-error has-feedback");
                    $formControlFeedback.removeClass('fa fa-close');
                }
            }
        }
    };
});