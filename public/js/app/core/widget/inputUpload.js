/**
 * @author OnCode
 * Classe responsavel pela manipulacao do upload.
 */
define(['core/loader',
		'core/util/model',
		'core/util/reloadContent',
		'core/lib/uploadForm'], function(Loader, Model, ReloadContent, UploadForm) {
	
	function InputUpload() {
		Model.call(this);
		
		/**
		* File Input
		*/
		this.field = null;

		/**
		* Armazena o valor final da imagem/arquivo anexado.
		*/
		this.resultInput = "";

		/**
		* Caso necessario colocar em algum campo o nome amigavel do arquivo.
		*/
		this.resultFileName = "";

		/**
		* Caso necessario colocar em algum campo o nome original do arquivo.
		*/
		this.resultName = "";

		/**
		*  Determina se e possivel atualizar o campo nome do arquivo.
		*/
		this.enableUpdateFileName = false;

		/**
		* Representa o botao que dispara a acao de clique em um input file.
		* Pode ser utilizado principalmente para customizar o estilo do file.
		*/
		this.buttonSelect = null;

		/**
		* Botao que realiza a remocao da imagem corrente.
		*/
		this.buttonRemove = null;

		/**
		* Url que recebera o arquivo, sempre que o mesmo for selecionado
		*/
		this.url = null;
		
		/**
		* Evento que irá disparar o upload
		*/
		this.attachEvent = "change";

		/**
		* Habilita o form reset após realizar o upload do arquivo
		*/
		this.enableFormReset = false;
		
		/**
		* Identifica se e necessario utilizar frame para o upload. Aplicado para navegadores antigos.
		*/
		this.useIframe = false;

		/**
		* Identifica se o upload e relativo a imagem.
		*/
		this.isImage = true;
		
		/**
		* Identifica a imagem default para quando nao existir nenhuma.
		*/
		this.emtpyImage = null;

		/**
		* Lista de paginas a serem recarregadas posterior ao formulario ser submetido.
		*/
		this.reloadContents = new Array();
				
		/**
		* Especifica um output para o retorno. (Div)
		*/
		this.dataContent = "";
		
		/**
		* Informacoes relativas a progresso do upload
		*/
		this.progressContent = null;
		this.progressBar = null;
		this.progressMessage = null;
				
		return (this);
	}
	
	InputUpload.prototype = Object.create(Model.prototype);
	
	/**
	* @param obj Objeto principal, injetado pela classe loader. Representa o elemento no qual ira ser manipulado
	* Metodo construtor.
	*/
	InputUpload.prototype.init = function(obj) {
		this.readDeclarative(obj);
		
		this.activeButtonSelect();
		this.activeButtonRemove();
		this.activeUpload();
		this.managerButtons();
	};

	/**
	* @param obj Elemento no qual serao extraidas as informacoes.
	* Metodo responsavel por ler as tag's e preencher os atributos da classe
	*/
	InputUpload.prototype.readDeclarative = function(obj) {	
		this.field = obj;
		
		this.resultInput = Loader.attributeRead(obj, "data-onc-result-input", this.resultInput);
		this.resultFileName = Loader.attributeRead(obj, "data-onc-result-file-name", this.resultFileName);
		this.resultName = Loader.attributeRead(obj, "data-onc-result-name", this.resultName);
		this.buttonSelect = Loader.attributeRead(obj, "data-onc-button-select", this.buttonSelect);
		this.buttonRemove = Loader.attributeRead(obj, "data-onc-button-remove", this.buttonRemove);
		this.url = Loader.attributeRead(obj, "data-onc-url", this.url);
		this.reloadContents = Loader.attributeReadList(obj, "data-onc-reload-contents", null);		
		this.attachEvent = Loader.attributeRead(obj, "data-onc-attach-event", this.attachEvent);
		this.enableFormReset = Loader.attributeReadBoolean(obj, "data-onc-enable-form-reset", this.enableFormReset);
		
		this.dataContent = Loader.attributeRead(obj, "data-onc-content", this.dataContent);
		this.isImage = Loader.attributeReadBoolean(obj, "data-onc-is-image", this.isImage);
		this.emtpyImage = Loader.attributeRead(obj, "data-onc-empty-image", this.emptyImage);

		this.progressBar = Loader.attributeRead(obj, "data-onc-progress-bar", this.progressBar);
		this.progressMessage = Loader.attributeRead(obj, "data-onc-progress-message", this.progressMessage);
		this.progressContent = Loader.attributeRead(obj, "data-onc-progress-content", this.progressContent);
		
		//Caso exista o FileReader o navegador possui suporte upload sem iframe.
		if (typeof(window.FileReader) == 'undefined') {
			this.useIframe = true;
	    }
	};
	
	/**
	* Metodo responsavel por personalizar o upload.
	*/
	InputUpload.prototype.activeButtonRemove = function() {
		var _this = this;

		$(_this.buttonRemove).click(function(e) {
			_this.clearInput();
			
			if(_this.isImage) {
				var image = $("<img />").attr("src", _this.emtpyImage);
				$(_this.dataContent).html(image);
			}

			$(_this.resultInput).val("");

			if(_this.resultFileName != "") {
				$(_this.resultFileName).val("");
			}

			if(_this.resultName != "") {
				$(_this.resultName).val("");
			}

			_this.managerButtons();
		});
	};

	/**
	* Metodo responsavel por personalizar o upload.
	*/
	InputUpload.prototype.activeButtonSelect = function() {
		var _this = this;

		$(_this.buttonSelect).click(function(e) {
			$(_this.field).trigger("click");
			e.preventDefault();
		});
	};

	/**
	* Metodo responsavel por ativar a acao de upload.
	*/
	InputUpload.prototype.activeUpload = function() {
		var _this = this;
		
		$(this.field).on(this.attachEvent, function() {
			var domProgressContent = $(_this.progressContent);
			
			domProgressContent.show();
			
			$(this).closest("form").ajaxSubmit({
				url: _this.url,
				iframe: _this.useIframe,
				beforeSend : function() {
					_this.beforeSend();
				},
				uploadProgress : function(event, position, total, percentComplete) {					
					_this.uploadProgress(event, position, total, percentComplete);
				},
				complete : function(xhr) {
					_this.complete(xhr);
				}, 
                success: function(data) {
                	_this.success(data);
                }
			});			
		});
	};

	InputUpload.prototype.managerButtons = function() {
		var domButtonRemove = $(this.buttonRemove);
		var domResultInput = $(this.resultInput);
		var domButtonSelect = $(this.buttonSelect);

		if(domButtonRemove[0] && domResultInput[0]) {
			if(domResultInput.val() == "") {
				domButtonRemove.hide();
				domButtonSelect.show();
			} else {
				domButtonRemove.show();
				domButtonSelect.hide();
			}
		}
	};

	InputUpload.prototype.clearInput = function(e) {
		var domField = $(this.field);

		//ie8+ doesn't support changing the value of input with type=file so clone instead
		if (navigator.userAgent.match(/msie/i)){ 
		  	var fieldClone = domField.clone();

		  	domField.unbind(this.attachEvent)
		  		    .replaceWith(function() {
		        return fieldClone;
		    });
		  	
			this.field = fieldClone;
			
			this.activeEvent();
		} else {
	  		domField.val("");
		}
    };
	
	/**
	* @param event 
	* @param position
	* @param total
	* @param percentComplete
	*
	* Metodo realiza a alteracao da barra de progresso.
	*/
	InputUpload.prototype.uploadProgress = function(event, position, total, percentComplete) {
		var domProgressBar = $(this.progressBar);
		var domProgressMessage = $(this.progressMessage);
		
		domProgressMessage.html(percentComplete + '%');
		domProgressBar.css("width", percentComplete + '%');
	
		if(percentComplete == 100 && this.isImage == true) {
			domProgressMessage.html("Redimensionando Imagens...");
		} else if(percentComplete == 100) {
			domProgressMessage.html("Processando Arquivo...");
		}
	};

	InputUpload.prototype.beforeSend = function() {
		var domProgressBar = $(this.progressBar);
		var domProgressMessage = $(this.progressMessage);
		
		if(this.useIframe == true) {
			domProgressMessage.html("Aguarde...");
			domProgressBar.css("width", "100%");
		} else {
			domProgressMessage.html("0%");
			domProgressBar.css("width", "0%");
		}
	};
	
	InputUpload.prototype.complete = function(xhr) {};
	
	/**
	* @param data Valor de retorno da requisicao realizada
	* Metodo responsavel por tratar resposta com sucesso.
	*/
	InputUpload.prototype.responseSuccess = function(data) {
	};
	
	/**
	* @param data Valor de retorno da requisicao realizada
	* Metodo responsavel por tratar resposta com error.
	*/
	InputUpload.prototype.responseError = function(data) {
	};

	/**
	* @param data Valor de retorno da requisicao realizada
	* Metodo responsavel por identificar se a requisicao ajax foi realizada com sucesso
	*/
	InputUpload.prototype.responseIsSuccess = function(data) {
		if($(data).find("success").text() == "true") {
			return true;
		} else {
			return false;
		}
	};

	/**
	* @param isSuccess Verifica se foi realizada com sucesso a operacao.
	* Metodo responsavel por exeutar a atualizacao dos campos pos processamento
	*/
	InputUpload.prototype.executeReloadContents = function(isSuccess) {
		if(this.reloadContents != null && isSuccess) {
			var reloadContentInstance = new ReloadContent();
			
			reloadContentInstance.contents = this.reloadContents;
			reloadContentInstance.execute();
		}
	};

	InputUpload.prototype.getFileName = function() {
		var fileName = $(this.field).val();
		fileName = fileName.split('\\').pop();
		fileName = fileName.substring(0, fileName.lastIndexOf("."));
		return fileName;
	};

	InputUpload.prototype.getFriendlyFileName = function() {
		var fileName = this.getFileName();
		return fileName.toLowerCase().replace(/-/g, ' ').replace(/[^\w ]+/g,'').replace(/ +/g,'-');
	};
	
	/**
	* @param data Valor de retorno da requisicao realizada
	* Metodo responsavel por tratar a resposta da requisicao.
	*/
	InputUpload.prototype.success = function(data) {
		var domProgressContent = $(this.progressContent);
		var domProgressMessage = $(this.progressMessage);
		var domResultInput = $(this.resultInput);
		var isSuccess = true;
    	
    	if(this.responseIsSuccess(data)) {    		
    		if(this.isImage) {
    			if(this.dataContent != null) {
					var image = $("<img />").attr("src", $(data).find("folder").text() + "/" + $(data).find("image").text() + "?time=" + new Date().getTime());
    				
    				$(this.dataContent).html(image.get());
    	    	}
    		} 
			
    		if(this.enableFormReset) {
				$(this.field).closest('form').get(0).reset();
				$(this.field).closest('form').find("select").change();
			}

			if(domResultInput[0]) {
				domResultInput.val($(data).find("image").text());
			}

			if(this.resultFileName != "") {
				$(this.resultFileName).val(this.getFriendlyFileName());
			}

			if(this.resultName != "") {
				$(this.resultName).val(this.getFileName());
			}
		
			this.responseSuccess(data);
		} else {
			isSuccess = false;			

			this.responseError(data);
		}
    	
    	domProgressMessage.html("100%");
    	domProgressContent.hide();
    	
    	this.executeReloadContents(isSuccess);

    	this.clearInput();
    	this.managerButtons();
   	};
	
	return InputUpload;
});