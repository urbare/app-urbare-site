/**
 * @author OnCode
 *
 */
define(['core/loader',
		'core/util/sender',
		'core/util/reloadContent',
		'core/util/redirectUrl',
		'core/util/model'], function(Loader, Sender, ReloadContent, RedirectUrl, Model) {
	
	'use strict';

	function Href() {
		Model.call(this);
		
		/**
		* Elemento principal, responsavel pelas informacoes da classe
		*/
		this.$element = "";

		/**
		* Especifica um output para o retorno. (Div)
		*/
		this.$dataContent = "";

		/**
		* Verifica se o formulario esta disponivel para ser submetido.
		*/
		this.available = true;
		
		/**
		* Elemento identifica a navegacao ajax
		*/
		this.hash = null;

		/**
		* Caso nao seja um link, deve ser passado o campo URL.
		*/
		this.url = null;
		
		/**
		* Evento que ira disparar o form submit
		*/
		this.attachEvent = "click";

		/**
		* Atributo identifica se o botao esta sendo processado. Caso sim
		* o botao e desativado.
		*/
		this.lockedButton = 'data-onc-disabled';

		/**
		* Tipo de requisicao
		*/
		this.method = "get";
		
		/**
		* Retorno da url relativo a acao do link
		*/
		this.returnType = "html";
		
		/**
		* Identifica se a div de ouput sera substituida ou somente seu conteudo sera trocado
		*/
		this.replaceContent = false;
		
		/**
		* Lista de paginas a serem recarregadas posterior ao formulario ser submetido.
		*/
		this.reloadContents = [];

		/**
		* Identifica se apos clicar no link o mesmo devera ser confirmado antes
		* da execucao da acao.
		*/
		this.isConfirm = false;
		
		return (this);
	}
	
	Href.prototype = Object.create(Model.prototype);
	
	/**
	* @param obj Objeto principal, injetado pela classe loader. Representa o elemento no qual ira ser manipulado
	* Metodo construtor.
	*/
	Href.prototype.init = function(obj) {
		this.readDeclarative(obj);
		
		this.click();
	};
	
	/**
	* @param obj Elemento no qual serao extraidas as informacoes.
	* Metodo responsavel por ler as tag's e preencher os atributos da classe
	*/
	Href.prototype.readDeclarative = function(obj) {
		this.url = Loader.attributeRead(obj, "data-onc-url", this.url);
		this.hash = Loader.attributeRead(obj, "data-onc-hash", this.hash);
		this.method = Loader.attributeRead(obj, "data-onc-method", this.method);
		this.attachEvent = Loader.attributeRead(obj, "data-onc-attach-event", this.attachEvent);
		this.returnType = Loader.attributeRead(obj, "data-onc-return-type", this.returnType);
		this.replaceContent = Loader.attributeReadBoolean(obj, "data-onc-replace-content", this.replaceContent);
		this.isConfirm =  Loader.attributeReadBoolean(obj, "data-onc-is-confirm", this.isConfirm);
		
		var objReloadContents = Loader.attributeRead(obj, "data-onc-reload-contents", null),
			dataContent = Loader.attributeRead(obj, "data-onc-data-content", this.$dataContent);
		
		if(objReloadContents !== null) {
			this.reloadContents = objReloadContents.split(",");
		}

		if(dataContent !== null) {
			this.$dataContent = $(dataContent);
		}

		this.$element = $(obj);
	};
	
	/**
	* Metodo le em tempo de execucao a tag data-onc-available, responsavel
	* por dizer se o formulario esta disponivel para ser submetido ou nao.
	*/
	Href.prototype.isAvailable = function() {
		return Loader.attributeReadBoolean(this.button, "data-onc-available", this.available);
	};
	
	/**
	* Metodo chamado antes da requisicao ser enviada
	*/
	Href.prototype.beforeSend = function() {
		return true;
	};
	
	/**
	* @param data Valor de retorno da requisicao realizada
	* Metodo responsavel por tratar a resposta da requisicao.
	*/
	Href.prototype.success = function(data) {
		if(this.returnType == 'json') {
            if(this.executeRedirectUrl(data)) {
                return;
            }

            this.responseJsonSuccess(data);
		} else if (this.returnType == 'html') {
			if(typeof this.$dataContent !== 'undefined' && this.$dataContent !== null) {
				if(this.replaceContent) {
					this.$dataContent.replaceWith(data);
				} else {
					this.$dataContent.html(data);
				}
				
				if(this.reloadContents === null || this.reloadContents.length <= 0) {
					Loader.reload();
				}
			}
				
			this.responseHtmlSuccess(data);
		}
	};

    /**
     * @param data Valor de retorno da requisicao realizada
     * Metodo responsavel por tratar a resposta da requisicao.
     */
    Href.prototype.error = function(data, xhr, ajaxOptions, thrownError) {
        if(this.returnType == 'json' && xhr.status === 400) {
            this.responseJsonError(data);
        }
    };

	/**
	* @param data Valor de retorno da requisicao realizada
	* Metodo responsavel por tratar resposta HTML.
	*/
	Href.prototype.responseHtmlSuccess = function(data) {
	};
	
	/**
	* @param data Valor de retorno da requisicao realizada
	* Metodo responsavel por tratar resposta JSON com sucesso.
	*/
	Href.prototype.responseJsonSuccess = function(data) {
	};
	
	/**
	* @param data Valor de retorno da requisicao realizada
	* Metodo responsavel por tratar resposta JSON com error.
	*/
	Href.prototype.responseJsonError = function(data) {
	};
	
	/**
	* @param data Valor de retorno da requisicao realizada
	* @p
	* Metodo responsavel por identificar se a requisicao ajax foi realizada com sucesso
	*/
	Href.prototype.responseJsonRedirect = function(data) {};

	/**
	* @param data Retorno da requisicao efetuada
	* @return Caso o elemento seja 
	* Metodo responsavel por verificar se e necessario o redirecionamento
	*/
	Href.prototype.executeRedirectUrl = function(data) {
		var redirectUrlInstance = new RedirectUrl();

		this.responseJsonRedirect(data);
		
		if(typeof data !== 'undefined' && data !== null) {
			if(typeof data.redirectUrl !== 'undefined' && data.redirectUrl !== null) {
				redirectUrlInstance.url = data.redirectUrl;
			}
			
			if(typeof data.redirectHash !== 'undefined' && data.redirectHash !== null) {
				redirectUrlInstance.hash = data.redirectHash;
			}
			
			if(typeof data.redirectIsAjax !== 'undefined' && data.redirectIsAjax !== null) {
				redirectUrlInstance.isAjax = data.redirectIsAjax;
			}
		}

		if(redirectUrlInstance.hasRedirect(this.$element)) {
			redirectUrlInstance.execute(this.$element);
		
			if(!redirectUrlInstance.isAjax) {
				return true;
			}
		}

		return false;
	};
	
	/**
	* @param isSuccess Verifica se foi realizada com sucesso a operacao.
	* Metodo responsavel por exeutar a atualizacao dos campos pos processamento
	*/
	Href.prototype.executeReloadContents = function() {
		if(this.reloadContents !== null) {
			var reloadContentInstance = new ReloadContent();
			
			reloadContentInstance.contents = this.reloadContents;
			reloadContentInstance.execute();
		}
	};
	
	/**
	* Metodo chamado apos a requisicao ser completada.
	*/
	Href.prototype.complete = function() {
		this.$element.removeClass(this.lockedButton);
	};
	
	/**
	* Metodo responsavel por tratar os eventos
	*/
	Href.prototype.click = function() {
		var _this = this;
		
		_this.$element.on(this.attachEvent, function(e) {
			if(_this.isAvailable()) {
				if(_this.isConfirm) {
					_this.confirm(e);
				} else {
					_this.logicSend(e);
				}
			}
			
			return false;
		});
	};
	
	/**
	* Metodo responsavel por tratar a confirmacao caso esteja ativada
	*/
	Href.prototype.confirm = function(e) {
		if(confirm(Loader.attributeRead(this.$element, "data-onc-confirm-message", "Confirmar execucao?"))) {
			this.logicSend(e);
		}
		
		return false;
	};
	
	Href.prototype.getSendUrl = function() {
		if(this.url === null) {
			return this.$element.attr("href");
		} else {
			return Loader.attributeRead(this.$element, "data-onc-url", this.url);
		}
	};

	/**
	* @param e Objeto contem informacoes do evento realizado
	* Metodo que executa o envio da requisicao para o servidor.
	*/
	Href.prototype.logicSend = function(e) {
		e.stopPropagation();
		
		var _this = this;

		if(!_this.$element.hasClass(_this.lockedButton)) {
			
			_this.$element.addClass(_this.lockedButton);
			
			var senderInstance = new Sender();
			
			senderInstance.method = _this.method;
			senderInstance.urlCall = _this.getSendUrl();
			
			if(_this.hash !== null) {
				window.history.pushState(null, null, _this.hash);
			}
			
			senderInstance.returnType = _this.returnType;

            senderInstance.cbSuccess = function(data) {
                _this.success(data);
                _this.executeReloadContents();
            };

            senderInstance.cbError = function(data, xhr, ajaxOptions, thrownError) {
				_this.error(data, xhr, ajaxOptions, thrownError);
            };
			
			senderInstance.cbBeforeSend = function() {
				return _this.beforeSend();
			};
			
			senderInstance.cbComplete = function() {
				_this.complete();
			};
			
			senderInstance.send();
		}
	};
	
	return Href;
});
