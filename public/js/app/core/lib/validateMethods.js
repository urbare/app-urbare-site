﻿/*!
 * jQuery Validation Plugin v1.13.0
 *
 * http://jqueryvalidation.org/
 *
 * Copyright (c) 2014 Jörn Zaefferer
 * Released under the MIT license
 */
(function( factory ) {
  if ( typeof define === "function" && define.amd ) {
    define( ['core/lib/validate'], factory );
  } else {
    factory( jQuery );
  }
}(function() {
  
  (function() {

     function validateCNPJ(cnpj) {
        cnpj = jQuery.trim(cnpj);
        
        // DEIXA APENAS OS NÚMEROS
        cnpj = cnpj.replace('/','');
        cnpj = cnpj.replace('.','');
        cnpj = cnpj.replace('.','');
        cnpj = cnpj.replace('-','');

        var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
        digitos_iguais = 1;

        if (cnpj.length < 14 && cnpj.length < 15){
           return false;
        }
        for (i = 0; i < cnpj.length - 1; i++){
           if (cnpj.charAt(i) != cnpj.charAt(i + 1)){
              digitos_iguais = 0;
              break;
           }
        }

        if (!digitos_iguais){
           tamanho = cnpj.length - 2;
           numeros = cnpj.substring(0,tamanho);
           digitos = cnpj.substring(tamanho);
           soma = 0;
           pos = tamanho - 7;

           for (i = tamanho; i >= 1; i--){
              soma += numeros.charAt(tamanho - i) * pos--;
              if (pos < 2){
                 pos = 9;
              }
           }
           resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
           if (resultado != digitos.charAt(0)){
              return false;
           }
           tamanho = tamanho + 1;
           numeros = cnpj.substring(0,tamanho);
           soma = 0;
           pos = tamanho - 7;
           for (i = tamanho; i >= 1; i--){
              soma += numeros.charAt(tamanho - i) * pos--;
              if (pos < 2){
                 pos = 9;
              }
           }
           resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
           if (resultado != digitos.charAt(1)){
              return false;
           }
           return true;
        }else{
           return false;
        }
     }

     function validateCPF(value) {
        value = jQuery.trim(value);
        
        value = value.replace('.','');
        value = value.replace('.','');
        cpf = value.replace('-','');
        while(cpf.length < 11) cpf = "0"+ cpf;
        var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
        var a = [];
        var b = new Number;
        var c = 11;
        for (i=0; i<11; i++){
           a[i] = cpf.charAt(i);
           if (i < 9) b += (a[i] * --c);
        }
        if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
        b = 0;
        c = 11;
        for (y=0; y<10; y++) b += (a[y] * c--);
        if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }
        
        var retorno = true;
        if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) retorno = false;
        
        return retorno;
     }

     function convertBrDatesToCompare(value) {
        var dates = value.split('/');
        return dates[2] + dates[1] + dates[0];
     }
     
     function stripHtml(value) {
        return value.replace(/<.[^<>]*?>/g, ' ').replace(/&nbsp;|&#160;/gi, ' ').replace(/[.(),;:!?%#$'"_+=\/\-]*/g,'');
     }

      $.validator.addMethod("hex", function(value, element) {
          return this.optional(element) || /^#[a-f0-9]{6}$/i.test(value);
      }, "Valor hexadecimal incorreto. Ex. #ffffff");

      $.validator.addMethod("hexnumber", function(value, element) {
          return this.optional(element) || /^[a-f0-9]{6}$/i.test(value);
      }, "Valor hexadecimal incorreto. Ex. ffffff");

     $.validator.addMethod("maxWords", function(value, element, params) {
        return this.optional(element) || stripHtml(value).match(/\b\w+\b/g).length <= params;
     }, $.validator.format("Please enter {0} words or less."));

     $.validator.addMethod("minWords", function(value, element, params) {
        return this.optional(element) || stripHtml(value).match(/\b\w+\b/g).length >= params;
     }, $.validator.format("Please enter at least {0} words."));

     $.validator.addMethod("rangeWords", function(value, element, params) {
        var valueStripped = stripHtml(value);
        var regex = /\b\w+\b/g;
        return this.optional(element) || valueStripped.match(regex).length >= params[0] && valueStripped.match(regex).length <= params[1];
     }, $.validator.format("Please enter between {0} and {1} words."));

     $.validator.addMethod("nowhitespace", function(value, element) {
        return this.optional(element) || /^\S+$/i.test(value);
     }, "Não é permitido espaços em branco.");

     $.validator.addMethod("alphanumeric", function(value, element) {
        return this.optional(element) || /^\w+$/i.test(value);
     }, "Letters, numbers, and underscores only please");

     $.validator.addMethod("variable", function(value, element) {
        return this.optional(element) || /^[a-zA-Z]+$/i.test(value);
     }, "Letters or punctuation only please");

     $.validator.addMethod("letterswithbasicpunc", function(value, element) {
        return this.optional(element) || /^[a-z\-.,()'"\s]+$/i.test(value);
     }, "Letters or punctuation only please");

     $.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z]+$/i.test(value);
     }, "Letras somente");

     $.validator.addMethod("integer", function(value, element) {
        return this.optional(element) || /^-?\d+$/.test(value);
     }, "A positive or negative non-decimal number please");

     $.validator.addMethod("login", function(value, element) {
        return this.optional(element) || /^[a-zA-Z0-9._]+$/i.test(value);
     }, "Informe um login válido");

     $.validator.addMethod("key", function(value, element) {
        return this.optional(element) || /^[a-zA-Z0-9._\-]+$/i.test(value);
     }, "Informe uma chave válida");

     $.validator.addMethod("friendlyurl", function(value, element) {
        return this.optional(element) || /^[a-zA-Z0-9._,\-]+$/i.test(value);
     }, "Informe uma url amigavel válida");

     // same as url, but TLD is optional
     $.validator.addMethod("url2", function(value, element) {
        return this.optional(element) || /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)*(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
     }, $.validator.messages.url);

     $.validator.addMethod("nowhitespace", function(value, element) {
        return this.optional(element) || /^\S+$/i.test(value);
     }, "No white space please");

     $.validator.addMethod("filename", function(value, element) {
        return this.optional(element) || /^[a-zA-Z0-9._\-]+$/i.test(value);
     }, "Informe um nome de arquivo válido");

     $.validator.addMethod("cpf", function(value, element) {
         return this.optional(element) || validateCPF(value);
     }, "Informe um CPF válido."); // Mensagem padrão 

     $.validator.addMethod("cnpj", function(value, element) {
        return this.optional(element) || validateCNPJ(value);
     }, "Informe um CNPJ válido."); // Mensagem padrão 

     $.validator.addMethod("cpfcnpj", function(value, element) {
        if (value.length <= 11) {
           return this.optional(element) || validateCPF(value);
        }

        return this.optional(element) || validateCNPJ(value);
     }, "Informe um CPF ou CNPJ válido."); // Mensagem padrão 

     $.validator.addMethod("date", function(value, element) {
        if(value.indexOf("-") != -1) {
           var dateSplit = value.split("-");
           value = dateSplit[2] + "/" + dateSplit[1] + "/" + dateSplit[0];
        }

        return (this.optional(element) || /^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[0-2])\/((1[2-9]|[2-9][0-9])[0-9]{2})$/.test(value));
     }, "Informe uma data válida");

     $.validator.addMethod("dateBrGreaterThan", function(value, element, param) {
        var compareValue = param;
        value = value.replace(new RegExp('[_]','g'), "");   

        if(isNaN(param)) {
            compareValue = $(param).val().replace(new RegExp('[_]','g'), "");
        }
        
        if(value == null || value.length < 10 || compareValue == null || compareValue.length < 10) {
            return true;
        }

        return this.optional(element) || convertBrDatesToCompare(value) > convertBrDatesToCompare(compareValue);
     }, "Data Inválida");

     $.validator.addMethod("dateBrGreaterThanOrEqual", function(value, element, param) {
        var compareValue = param;
        value = value.replace(new RegExp('[_]','g'), "");   

        if(isNaN(param)) {
            compareValue = $(param).val().replace(new RegExp('[_]','g'), "");
        }
        
        if(value == null || value.length < 10 || compareValue == null || compareValue.length < 10) {
           return true;
        }

        return this.optional(element) || convertBrDatesToCompare(value) >= convertBrDatesToCompare(compareValue);
     }, "Data Inválida");

     $.validator.addMethod("dateBrLessThan", function(value, element, param) {
        var compareValue = param;
        value = value.replace(new RegExp('[_]','g'), "");   
       
        if(isNaN(param)) {
            compareValue = $(param).val().replace(new RegExp('[_]','g'), "");
        }
        
        if(value == null || value.length < 10 || compareValue == null || compareValue.length < 10) {
            return true;
        }

        return this.optional(element) || convertBrDatesToCompare(value) < convertBrDatesToCompare(compareValue);
     }, "Data Inválida");

     $.validator.addMethod("dateBrLessThanOrEqual", function(value, element, param) {
        var compareValue = param;
        value = value.replace(new RegExp('[_]','g'), "");   
  
        if(isNaN(param)) {
            compareValue = $(param).val().replace(new RegExp('[_]','g'), "");
        }
         
        if(value == null || value.length < 10 || compareValue == null || compareValue.length < 10) {
            return true;
        }

        return this.optional(element) || convertBrDatesToCompare(value) <= convertBrDatesToCompare(compareValue);
     }, "Data Inválida");

     $.validator.addMethod("datetimebr", function(value, element) {
        return this.optional(element) || /^((((31\/(0?[13578]|1[02]))|((29|30)\/(0?[1,3-9]|1[0-2])))\/(1[6-9]|[2-9]d)?d{2})|(29\/0?2\/(((1[6-9]|[2-9]d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))|(0?[1-9]|1d|2[0-8])\/((0?[1-9])|(1[0-2]))\/((1[6-9]|[2-9]d)?d{2})) (20|21|22|23|[0-1]?d):[0-5]?d:[0-5]?d$/.test(value);
     }, "Informe uma data e uma hora válida");

     $.validator.addMethod("time", function(value, element) {
        return this.optional(element) || /^([01]\d|2[0-3])(:[0-5]\d){1,2}$/.test(value);
     }, "Please enter a valid time, between 00:00 and 23:59");

     $.validator.addMethod("time12h", function(value, element) {
        return this.optional(element) || /^((0?[1-9]|1[012])(:[0-5]\d){1,2}(\ ?[AP]M))$/i.test(value);
     }, "Please enter a valid time in 12-hour am/pm format");

     $.validator.addMethod("zipcode", function(value, element) {
        value = value.replace(new RegExp('[_]','g'), "");

        if(value == '-') {
            value = '-';
        }
        
        return this.optional(element) || /^[0-9]{5}-[0-9]{3}/.test(value);
     }, "Informe um cep válido");

     $.validator.addMethod("phonebr", function(value, element) {
        value = value.replace(new RegExp('[_]','g'), "");
        return this.optional(element) || /^[(][0-9]{2}[)] [0-9]{4,5}[-][0-9]{4,5}$/.test(value);
     }, "Informe um telefone válido");

     $.validator.addMethod("moneybr", function(value, element) {
        return this.optional(element) || /^(?:[1-9](?:[\d]{0,2}(?:\.[\d]{0,2})*|[\d]+)|0)(?:,[\d]{0,2})?$/.test(value);
     }, "Informe um valor válido");

     $.validator.addMethod("passwordeasy", function(value, element) {
        return this.optional(element) || /((?=.*[a-zA-Z0-9]).{6,20})/.test(value);
     }, "Informe um valor válido");

     $.validator.addMethod("passwordmedium", function(value, element) {
        return this.optional(element) || /((?=.*\d)(?=.*[a-zA-Z]).{6,20})/.test(value);
     }, "Informe um valor válido");

     $.validator.addMethod("passwordhard", function(value, element) {
        return this.optional(element) || /((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})/.test(value);
     }, "Informe um valor válido");

     $.validator.addMethod("greaterThanOrEqual", function(value, element, param) {
          var compareValue = param;

          if(isNaN(param)) {
              compareValue = $(param).val();
          }
          
          if(typeof compareValue === 'undefined' || compareValue == null || compareValue == '') {
              return true;
          }
          
          return this.optional(element) || (parseFloat(compareValue) <= parseFloat(value));
     }, "Informe um valor válido");

     $.validator.addMethod("greaterThan", function(value, element, param) {
          var compareValue = param;

          if(isNaN(param)) {
              compareValue = $(param).val();
          } 
          
          if(typeof compareValue === 'undefined' || compareValue == null || compareValue == '') {
              return true;
          }
          
          return this.optional(element) || (parseFloat(compareValue) < parseFloat(value));
     }, "Informe um valor válido");

     $.validator.addMethod("lessThan", function(value, element, param) {
          var compareValue = param;

          if(isNaN(param)) {
              compareValue = $(param).val();
          }
          
          if(typeof compareValue === 'undefined' || compareValue == null || compareValue == '') {
              return true;
          }
          
          return this.optional(element) || (parseFloat(compareValue) > parseFloat(value));
     }, "Informe um valor válido");

     $.validator.addMethod("lessThanOrEqual", function(value, element, param) {
          var compareValue = param;

          if(isNaN(param)) {
              compareValue = $(param).val();
          }
          
          if(typeof compareValue === 'undefined' || compareValue == null || compareValue == '') {
              return true;
          }
          
          return this.optional(element) || (parseFloat(compareValue) >= parseFloat(value));
     }, "Informe um valor válido");

     /*
      * Lets you say "at least X inputs that match selector Y must be filled."
      *
      * The end result is that neither of these inputs:
      *
      *  <input class="productinfo" name="partnumber">
      *  <input class="productinfo" name="description">
      *
      *  ...will validate unless at least one of them is filled.
      *
      * partnumber:  {require_from_group: [1,".productinfo"]},
      * description: {require_from_group: [1,".productinfo"]}
      *
      */
     $.validator.addMethod("require_from_group", function(value, element, options) {
        if( typeof options === 'string' ) {
            options = options.split(',');
        }

        var validator = this;
        var selector = options[1];
        var validOrNot = $(selector, element.form).filter(function() {
           return validator.elementValue(this);
        }).length >= options[0];

        if(!$(element).data('being_validated')) {
           var fields = $(selector, element.form);
           fields.data('being_validated', true);
           fields.valid();
           fields.data('being_validated', false);
        }
        return validOrNot;
     }, $.validator.format("Please fill at least {0} of these fields."));

     /*
      * Lets you say "either at least X inputs that match selector Y must be filled,
      * OR they must all be skipped (left blank)."
      *
      * The end result, is that none of these inputs:
      *
      *  <input class="productinfo" name="partnumber">
      *  <input class="productinfo" name="description">
      *  <input class="productinfo" name="color">
      *
      *  ...will validate unless either at least two of them are filled,
      *  OR none of them are.
      *
      * partnumber:  {skip_or_fill_minimum: [2,".productinfo"]},
      *  description: {skip_or_fill_minimum: [2,".productinfo"]},
      * color:       {skip_or_fill_minimum: [2,".productinfo"]}
      *
      */
     $.validator.addMethod("skip_or_fill_minimum", function(value, element, options) {
        var validator = this,
           numberRequired = options[0],
           selector = options[1];
        var numberFilled = $(selector, element.form).filter(function() {
           return validator.elementValue(this);
        }).length;
        var valid = numberFilled >= numberRequired || numberFilled === 0;

        if(!$(element).data('being_validated')) {
           var fields = $(selector, element.form);
           fields.data('being_validated', true);
           fields.valid();
           fields.data('being_validated', false);
        }
        return valid;
     }, $.validator.format("Please either skip these fields or fill at least {0} of them."));

     // Accept a value from a file input based on a required mimetype
     $.validator.addMethod("accept", function(value, element, param) {
        // Split mime on commas in case we have multiple types we can accept
        var typeParam = typeof param === "string" ? param.replace(/\s/g, '').replace(/,/g, '|') : "image/*",
        optionalValue = this.optional(element),
        i, file;

        // Element is optional
        if (optionalValue) {
           return optionalValue;
        }

        if ($(element).attr("type") === "file") {
           // If we are using a wildcard, make it regex friendly
           typeParam = typeParam.replace(/\*/g, ".*");

           // Check if the element has a FileList before checking each file
           if (element.files && element.files.length) {
              for (i = 0; i < element.files.length; i++) {
                 file = element.files[i];

                 // Grab the mimetype from the loaded file, verify it matches
                 if (!file.type.match(new RegExp( ".?(" + typeParam + ")$", "i"))) {
                    return false;
                 }
              }
           }
        }

        // Either return true because we've validated each file, or because the
        // browser does not support element.files and the FileList feature
        return true;
     }, $.validator.format("Please enter a value with a valid mimetype."));

     // Older "accept" file extension method. Old docs: http://docs.jquery.com/Plugins/Validation/Methods/accept
     $.validator.addMethod("extension", function(value, element, param) {
        param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpe?g|gif";
        return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
     }, $.validator.format("Please enter a value with a valid extension."));

  }());

}));