/**
 * @author OnCode
 * 
 * Responsavel por manipular validacao e mensagens para todo o sistema. Classe padrao utilizada em 90% dos formularios.
 */
define(['core/loader',
		'core/widget/formSubmit',
		'core/util/model',
		'core/lib/validate',
		'core/lib/validateMethods',
		'app/view/helper/message',
		'app/view/helper/validator'], function(Loader, FormSubmit, Model, Validate, ValidateMethods, Message, Validator) {
	
	'use strict';

	function FormSubmitValidate() {
		Model.call(this);
		FormSubmit.call(this);

		this.validator = null;

		this.enableValidatorMessage = true;

		this.isCloseDialog = false;

		return (this);
	}
	
	FormSubmitValidate.prototype = Object.create(Model.prototype);
	
	/**
	* @param obj Objeto principal, responsavel pelas informacoes da classe
	* Metodo construtor
	*/
	FormSubmitValidate.prototype.init = function(obj) {
		FormSubmit.prototype.init.call(this, obj);

		this.validator = this.setConfigurations();

		this.showModalMessage();
		this.setCustomRules();
	};

	FormSubmitValidate.prototype.showModalMessage = function () {
		var _this = this;

		if(_this.enableValidatorMessage) {
			this.$element.on('click', function () {
				var i = 0,
					length = _this.validator.errorList.length,
					message = '';

				if (length > 0) {
					message += '<div class="cs-modal-error-validator">';
					message += '<p>Prezado Cliente, verifique os erros abaixo para enviar seu formulário</p>';

					for (i; i < length; i++) {
						if (_this.validator.errorList[i].message != undefined && _this.validator.errorList[i].message != null &&
							_this.validator.errorList[i].message != '') {
							message += '<p class="cs-modal-error-validator-list"><i class="fa fa-remove"></i>' + _this.validator.errorList[i].message + '</p>';
						}
					}

					message += '</div>';
					Message.showError(message);
				}
			});
		}
	};

	FormSubmitValidate.prototype.readDeclarative = function(obj) {
		FormSubmit.prototype.readDeclarative.call(this, obj);
		this.isCloseDialog = Loader.attributeReadBoolean(obj, "data-onc-is-dialog-close", this.isCloseDialog);
		this.enableValidatorMessage = Loader.attributeReadBoolean(obj, "data-onc-enable-validator-message", this.enableValidatorMessage);
	};

	FormSubmitValidate.prototype.responseJsonRedirect = function(data) {
		FormSubmit.prototype.responseJsonRedirect.call(this, data);

		var redirectContent = this.$element.attr("data-onc-redirect-content");

		if(typeof redirectContent === 'undefined' || redirectContent === null || redirectContent === '') {
			this.$element.attr("data-onc-redirect-content", ".tpl-content");
		}
	};

	/**
	* Funcao responsavel por realizar o reset de todos os campos do formulario.
	*/
	FormSubmitValidate.prototype.resetForm = function() {
		FormSubmit.prototype.resetForm.call(this);

		this.validator.resetForm();

		this.$form.find('.control-group')
			.removeClass("success error");
	};

	FormSubmitValidate.prototype.beforeSend = function() {
		if(!this.validator.checkForm()) {
			this.validator.showErrors();
			return false;
		}
		return true;
	};

	/**
	* @param data Valor de retorno da requisicao realizada
	* Metodo responsavel por tratar resposta JSON.
	*/
	FormSubmitValidate.prototype.responseJsonSuccess = function(data) {
		FormSubmit.prototype.responseJsonSuccess.call(this, data);

		if(this.isCloseDialog) {
			this.$element.closest('.modal').find('[data-dismiss]').click();
		}

		if(typeof data.message !== 'undefined' && data.message !== null) {
			Message.showSuccess(data.message);
		}
	};

	/**
	* @param data Valor de retorno da requisicao realizada
	* Metodo responsavel por tratar resposta JSON com error.
	*/
	FormSubmitValidate.prototype.responseJsonError = function(data) {
		FormSubmit.prototype.responseJsonError.call(this, data);

		var message = '';

		if(typeof data.fields !== 'undefined' && data.fields !== null) {
			var i,
				fieldLength = data.fields.length,
				errors = [];
				
			for(i = 0; i < fieldLength; i++) {
				if(!data.fields[i].success) {
					var name = data.fields[i].name,
						fieldError = this.$form.find('[name="'+name+'"]');

					if(fieldError.length > 0) {
						errors[name] = data.fields[i].message;
					} else {
						message += '<p>' + data.fields[i].message + '</p>';
					}
				}
			}

			this.validator.showErrors(errors);
		}
		
		if(typeof data.message !== 'undefined' && data.message !== null) {
			message += '<p>' + data.message + '</p>';
		}

		if(message !== '') {
			Message.showError(data.message);
		}
	};

	/*----------------------------------------------------
	* METODOS DE CONFIGURACOES DO PLUGIN VALIDATOR.
	*-------------------------------------------------------*/

	/**
	* Metodo utilizado para o componente adicionar configuracoes customizadas.
	*/
	FormSubmitValidate.prototype.setCustomRules = function() {
	};

	/**
	* Metodo principal responsavel pela gerencia de configuracoes do plugin validator.
	*/
	FormSubmitValidate.prototype.setConfigurations = function() {
		var _this = this;

		return _this.$form.validate({
			errorElement: "p",
			ignore: ':hidden:not(".disabled-ignore")',
			errorPlacement: function(error, element) {
				_this.setConfigurationError(error, element);
			},
			success: function(label, element) {
				_this.setConfigurationSuccess(label, element);
			},
			onkeyup: false
		});
	};

	/**
	* Metodo chamado quando o campo e preenchido corretamente
	*/
	FormSubmitValidate.prototype.setConfigurationSuccess = function(label, element) {
		Validator.addSuccess(element);
	};

	/**
	* Metodo chamado quando o campo e preenchido incorretamente
	*/
	FormSubmitValidate.prototype.setConfigurationError = function(error, element) {
		Validator.addError(element, error);
	};

	FormSubmitValidate.prototype = $.extend({}, Model.prototype, FormSubmit.prototype, FormSubmitValidate.prototype);
	
	return FormSubmitValidate;
});