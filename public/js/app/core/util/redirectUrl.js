define(['core/loader',
	    'core/util/sender', 
	    'core/util/model'], function(Loader, Sender, Model) {
	
	function RedirectUrl() {
		Model.call(this);
		
		this.url = null;

		this.isAjax = true;
		
		this.content = "";
		
		this.hash = null;
		
		this.replaceContent = false;

		return (this);
	}
	
	RedirectUrl.prototype = Object.create(Model.prototype);
	
	RedirectUrl.prototype.readDeclarative = function(obj) {
		this.url = Loader.attributeRead(obj, "data-onc-redirect-url", this.url);
		this.isAjax = Loader.attributeReadBoolean(obj, "data-onc-redirect-is-ajax", this.isAjax);
		this.replaceContent = Loader.attributeReadBoolean(obj, "data-onc-redirect-replace-content", this.replaceContent);
		this.content = Loader.attributeRead(obj, "data-onc-redirect-content", this.content);
		this.hash = Loader.attributeRead(obj, "data-onc-redirect-hash", this.hash);
	};
	
	RedirectUrl.prototype.hasRedirect = function(obj) {
		var dataRedirect = $(obj).attr("data-onc-redirect-url");
		
		if((typeof this.url !== 'undefined' &&  this.url !== null && this.url !== '') ||
			(typeof dataRedirect !== 'undefined' &&  dataRedirect !== null && dataRedirect !== '')) {
			return true;
		}

		return false;
	};

	RedirectUrl.prototype.execute = function(obj) {
		var _this = this;

		_this.readDeclarative(obj);
		
		if(_this.isAjax) {
			var senderInstance = new Sender();
				
			senderInstance.method = "get";
			senderInstance.urlCall = _this.url;
			senderInstance.returnType = "html";
			
			senderInstance.cbSuccess = function(data) {
				if(_this.replaceContent) {
					$(_this.content).replaceWith(data);
				} else {
					$(_this.content).html(data);
				}

				Loader.reload();
			};
			
			senderInstance.send();

			if(_this.hash != null) {
				window.history.pushState(null, null, _this.hash);
			}
		} else {
			window.location.href=this.url;
			return;
		}
	};
		
	return (RedirectUrl);
});
