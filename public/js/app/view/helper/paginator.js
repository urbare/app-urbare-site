define(["core/util/format/url"], function(UrlFormat) {
	return {
		getPaginatorType: function(paginationArea) {
			return (paginationArea.find("form")[0]) ? 'FORM' : 'LINK';
		},

		formPaginatorLogicSubmit: function(paginationArea, parameters) {
			for(var key in parameters) {
				console.log(parameters[key]);
				paginationArea.find("form input[name='"+key+"']").val(parameters[key]);
			}

			paginationArea.find("a:contains('1')").first().click();
		},

		linkPaginatorLogicSubmit: function(paginationArea, parameters) {
			var firstLink = paginationArea.find("a:contains('1')").first(),
				url = UrlFormat.getWithOutParameter(firstLink.attr("href")),
				hrefParameters = UrlFormat.decodeGetParameter(firstLink.attr("href")),
				hashUrl = null,
				finalParameters = '';

			if(firstLink.attr("data-onc-hash")) {
				hashUrl = UrlFormat.getWithOutParameter(firstLink.attr("data-onc-hash"));
			}

			finalParameters = this.groupParameters(hrefParameters, parameters);

			firstLink.attr("href", url + finalParameters);
			
			if(hashUrl)
				firstLink.attr("data-onc-hash", hashUrl + finalParameters);
			
			firstLink.click();
		},

		groupParameters: function(parametersA, parametersB) {
			var i = 0,
				finalParameters = "";

			for(var keyA in parametersA) {
				if(i === 0) {
					finalParameters += "?";
				} else {
					finalParameters += "&";
				}

				if(parametersB[keyA] !== undefined) {
					finalParameters +=  keyA + "=" + encodeURI(parametersB[keyA]);
				} else {
					finalParameters +=  keyA + "=" + encodeURI(parametersA[keyA]);
				}
				
				i++;
			}

			for(var keyB in parametersB) {
				if(parametersA[keyB] === undefined) {
					if(i === 0) {
						finalParameters += "?";
					} else {
						finalParameters += "&";
					}

					finalParameters +=  keyB + "=" + encodeURI(parametersB[keyB]);
				}
			}

			console.log(finalParameters);

			return finalParameters;
		}
	};
});