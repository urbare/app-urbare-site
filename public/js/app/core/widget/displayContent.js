/**
 * @author OnCode
 *
 */
define(['core/loader', "core/util/model"], function(Loader, Model) {
	
	function ButtonDisplayContent() {
		Model.call(this);
		
		/**
		* Evento que ira disparar a execucao.
		*/
		this.attachEvent = "click";

		/**
		* Elemento que realiza o disparo do evento
		*/
		this.element = null;

		/**
		* Elementos que serão desativados/ativados no momento da execucao do evento
		*/
		this.toggle = null;

		/**
		* Elementos que serao desativados no momento da execucao do evento
		*/
		this.hide = null;

		/**
		* Elementos que serao ativados no momento da execucao do evento.
		*/
		this.show = null;

		/**
		* Texto exibido quando estiver display = hide.
		*/
		this.hideText = null;

		/**
		* Texto exibido quando estiver display = block.
		*/
		this.showText = null;

		return (this);
	}
	
	ButtonDisplayContent.prototype = Object.create(Model.prototype);
	
	/**
	* @param obj Objeto principal, injetado pela classe loader. Representa o elemento no qual ira ser manipulado
	* Metodo construtor.
	*/
	ButtonDisplayContent.prototype.init = function(obj) {
		this.readDeclarative(obj);
		this.execute();
	};
	
	/**
	* @param obj Elemento no qual serao extraidas as informacoes.
	* Metodo responsavel por ler as tag's e preencher os atributos da classe
	*/
	ButtonDisplayContent.prototype.readDeclarative = function(obj) {
		this.attachEvent = Loader.attributeRead(obj, "data-onc-attach-event", this.attachEvent);

		this.toggle = Loader.attributeRead(obj, "data-onc-toggle", this.toggle);
		this.hide = Loader.attributeRead(obj, "data-onc-hide", this.hide);
		this.show = Loader.attributeRead(obj, "data-onc-show", this.show);
		this.showText = Loader.attributeRead(obj, "data-onc-show-text", this.showText);
		this.hideText = Loader.attributeRead(obj, "data-onc-hide-text", this.hideText);

		this.element = obj;
	};
	
	ButtonDisplayContent.prototype.execute = function() {
		var _this = this;

		$(_this.element).on(_this.attachEvent, function() {

			if(_this.toggle !== null) {
				_this.processToggle();

				if($(_this.toggle).css('display') == 'none') {
					_this.changeText('HIDE');
				} else {
					_this.changeText('SHOW');
				}
			}
			
			if(_this.hide !== null) {
				_this.processHide();
				_this.changeText('HIDE');
			}

			if(_this.show !== null) {
				_this.processShow();
				_this.changeText('SHOW');
			}
		});
	};

	ButtonDisplayContent.prototype.processToggle = function() {
		$(this.toggle).toggle();
	};

	ButtonDisplayContent.prototype.processShow = function() {
		$(this.show).show();
	};

	ButtonDisplayContent.prototype.processHide = function() {
		$(this.hide).hide();
	};

	ButtonDisplayContent.prototype.changeText = function(action) {
		var $element = $(this.element);
		
		if(this.showText !== null && this.hideText !== null) {
			if(action === 'HIDE') {
				$element.html(this.hideText);
			} else {
				$element.html(this.showText);
			}
		}
	};


	return ButtonDisplayContent;
});