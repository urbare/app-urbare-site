define(function() {
    return {
        error404: function(url, response, data) {
            $(".tpl-content").html(response);
        },
        error401: function(url, response, data) {
            window.location = BASE_URL;
        },
        error400: function(url, response, data) {
            $(".tpl-content").html(response);
        },
        error403: function(url, response,data) {
            window.location = BASE_URL;
        },
        error500: function(url, response, data) {
            $(".tpl-content").html(response);
        },
        error0: function(url, response, data) {
            alert("Não foi localizada nenhuma conexão com a internet");
        },
        error: function(url, response, data, error) {
            if (error === 'parsererror') {
                console.log('Error: Requested JSON parse failed.');
            } else if (error === 'timeout') {
                console.log('Error: Time out error.');
            } else if (error === 'abort') {}
        }
    };
});